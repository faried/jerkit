package main

import (
	"bytes"
	"encoding/hex"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// a function that takes a string and returns a string
type StringEncoder func(string) string

type Comic struct {
	Title         string     // tagline
	Char1, Char2  string     // characters
	Captions      [][]string // three rows of two characters talking
	BaseURL       string     // set to r.URL.Path
	ObfuscatedURL string     // for the obfuscated link
}

func (c Comic) String() string {
	return c.Encode("\n", func(s string) string { return s })
}

// encode for url
func (c Comic) Encode(sep string, enc StringEncoder) string {
	parts := []string{"op=cmake"}

	// title
	parts = append(parts, "tagline="+enc(c.Title))
	// characters
	parts = append(parts, "u1="+c.Char1)
	parts = append(parts, "u2="+c.Char2)
	// panels
	for i, num := range []string{"1, 2, 3"} {
		for j, ab := range []string{"a", "b"} {
			parts = append(parts, "c"+num+ab+"="+enc(c.Captions[i][j]))
		}
	}

	return strings.Join(parts, sep)
}

var (
	templates   *template.Template
	templatedir = flag.String("templatedir", "", "directory with the page templates")
	imagedir    = flag.String("imagedir", "", "directory with the image files")

	// format: [filename prefix, character name]
	characters = [][]string{
		[]string{"pants", "Pants"},
		[]string{"rands", "Rands"},
		[]string{"spigot", "Spigot"},
		[]string{"deuce", "Deuce"},
		[]string{"atandt", "Atandt"},
		[]string{"bung", "Bung"},
		[]string{"dick", "Dick"},
		[]string{"girl", "Girl"},
		[]string{"hanford", "Hanford"},
		[]string{"hooker", "Hooker"},
		[]string{"john", "John"},
		[]string{"net", "Net"},
		[]string{"ozone", "Ozone"},
		[]string{"the_precious", "The Precious"},
		[]string{"nobody", "Nobody"},
	}
)

func init() {
	flag.Parse()
	info, err := os.Stat(*imagedir)
	if err != nil || !info.IsDir() {
		log.Fatal("set --imagedir to a directory with images")
	}

	info, err = os.Stat(*templatedir)
	if err != nil || !info.IsDir() {
		log.Fatal("set --templatedir to a directory with templates")
	}

	tfuncs := template.FuncMap{
		"upper":   strings.ToUpper,
		"panel":   Panel,
		"caption": Caption,
		"scrub":   Scrub,
	}

	templates = template.Must(template.New("").Funcs(tfuncs).ParseGlob(path.Join(*templatedir, "*.html")))
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", HomepageHandler)
	r.HandleFunc("/images/{image}", ImageHandler)

	http.Handle("/", handlers.CombinedLoggingHandler(os.Stdout, r))
	http.ListenAndServe(":8080", nil)
}

// index page or call to script generator
func HomepageHandler(w http.ResponseWriter, r *http.Request) {

	op := r.FormValue("op")
	if op == "make" || op == "cmake" {
		StripHandler(w, r, op)
	} else {
		err := templates.ExecuteTemplate(w, "form.html", characters)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

// static images
func ImageHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	imagename := vars["image"]

	http.ServeFile(w, r, *imagedir+"/"+imagename)
}

func StripHandler(w http.ResponseWriter, r *http.Request, op string) {
	comic := getvars(r, op)

	err := templates.ExecuteTemplate(w, "strip.html", comic)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// template functions
func Panel(char1 string, char2 string, captions []string) template.HTML {
	m := map[string]string{
		"char1": char1,
		"char2": char2,
		"cap1":  captions[0],
		"dir1":  "l",
		"cap2":  captions[1],
		"dir2":  "r",
	}
	var b bytes.Buffer

	err := templates.ExecuteTemplate(&b, "panel.html", m)
	if err != nil {
		log.Fatal(err.Error())
		// http.Error(&b, err.Error(), http.StatusInternalServerError)
	}

	return template.HTML(b.String())
}

func Caption(caption, direction string) template.HTML {
	if len(caption) == 0 || strings.TrimSpace(caption) == "" {
		return template.HTML("")
	}

	m := map[string]string{
		"caption":   caption,
		"direction": direction,
	}
	var b bytes.Buffer

	err := templates.ExecuteTemplate(&b, "caption.html", m)
	if err != nil {
		log.Fatal(err.Error())
		// http.Error(&b, err.Error(), http.StatusInternalServerError)
	}

	return template.HTML(b.String())
}

func Scrub(name, direction string) template.HTML {
	fname := ""
	if len(name) > 0 && !strings.EqualFold(name, "nobody") {
		fname = path.Join("images/", name+"_"+direction+".gif")
	}

	return template.HTML(fmt.Sprintf("<img src=\"%s\" />", fname))
}

// misc functions from the php code
func getvars(r *http.Request, op string) (c Comic) {
	c.Captions = make([][]string, 3)
	for i := 0; i < 3; i++ {
		c.Captions[i] = make([]string, 2)
	}

	if op == "make" {
		c.Title = r.FormValue("tagline")
		for i, num := range []string{"1", "2", "3"} {
			for j, ab := range []string{"a", "b"} {
				c.Captions[i][j] = r.FormValue("c" + num + ab)
			}
		}
	} else {
		c.Title = hex2bin(r.FormValue("tagline"))
		for i, num := range []string{"1", "2", "3"} {
			for j, ab := range []string{"a", "b"} {
				c.Captions[i][j] = hex2bin(r.FormValue("c" + num + ab))
			}
		}
	}

	c.Char1 = r.FormValue("u1")
	c.Char2 = r.FormValue("u2")
	c.BaseURL = r.URL.Path
	c.ObfuscatedURL = obfuscatedurl(r, c)

	return
}

func obfuscatedurl(r *http.Request, c Comic) (url string) {
	return fmt.Sprintf("%s?%s", r.URL.Path,
		c.Encode("&", func(s string) string { return bin2hex(s) }))
}

// takes an ascii string and returns the hex representation
// hi+there becomes 6869207468657265
func bin2hex(s string) string {
	return hex.EncodeToString([]byte(s))
}

// reverse
func hex2bin(h string) string {
	b, err := hex.DecodeString(h)
	// TODO: do something smarter here
	if err != nil {
		log.Fatalf("hex2bin failed for %s\n", h)
	}

	return string(b)
}
